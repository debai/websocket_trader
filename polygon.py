
import requests
import json
import os
import pandas as pd
from datetime import datetime, timedelta
from dateutil import parser

API_KEY = os.environ['polygon_api_key']
start_date = "2021-01-01"
end_date = "2022-10-04"  # up to and including
symbol = "NVDA"
file_path = "/Users/kale/Desktop/stocks/data/1min/" + symbol + ".csv"
url = "https://api.polygon.io/v2/aggs/ticker/{symbol}/range/1/minute/{t1}/{t2}?adjusted=true&sort=asc&limit=50000&apiKey=" + API_KEY

td = timedelta(days=50)
ed = parser.parse(end_date)
t1 = parser.parse(start_date)
t2 = parser.parse(start_date)
t2 += td - timedelta(days=1)
bars = []

while True:
    request_url = url.format(url, symbol=symbol, t1=t1.strftime("%Y-%m-%d"), t2=t2.strftime("%Y-%m-%d"))
    request = requests.get(url=request_url)
    request = json.loads(request.content)

    if request["resultsCount"] > 0:
        bars += request["results"]
        print(request_url)

    if t2 >= ed:
        break
    elif t2 + td >= ed:
        t1 = t2 + timedelta(days=1)
        t2 = ed
    else:
        t1 += td
        t2 += td



df = pd.DataFrame(bars)
unix_epoch_times = list(df["t"])

dates = map(lambda x: datetime.fromtimestamp(x/1000), unix_epoch_times)
dates = map(lambda x: x.strftime("%Y-%m-%d %H:%M:%S"), dates)
df["date"] = list(dates)

df.drop_duplicates(inplace=True)
df.to_csv(file_path, index=False)


