import pandas as pd
import numpy as np

DATA_1MIN_FILE = "/Users/kale/Desktop/stocks/data/1min/TQQQ.csv"
TRADE_SIM_FILE = "trade_sim.csv"

# Read csvs
df = pd.read_csv(DATA_1MIN_FILE)


CASH = 100
COMMISSION = 0
BUY_FACTOR = 0.014

shares = 0
action = "Nothing"
cash = CASH
target_price = 0

trade_list = []  # actions: buy to open, buy to close, sell to open, sell to close
shares_list = []
cash_list = []

df["time"] = df["date"].str[11:]

# setting things up for the 1at iteration
row = df.loc[0, :]
v_last = row["v"]
o_last = row["o"]
c_last = row["c"]
h_last = row["h"]
l_last = row["l"]
trade_list += [action]
cash_list += [cash]
shares_list += [shares]
transact_price = 0
transact_price_last = 0

for index, row in df.iterrows():

    if index < 1:
        continue

    # print(index)
    v = row["v"]
    o = row["o"]
    c = row["c"]
    h = row["h"]
    l = row["l"]
    ts = row["time"]
    action = "Nothing"
    transact_price = (c_last * (1 - BUY_FACTOR))



    # buy conditon
    # ("04:15:00" < ts < "09:30:00" or "16:05:00" < ts < "19:30:00"):
    # ("09:45:00" < ts < "15:45:00"):
    if shares == 0 and l < transact_price and ("04:15:00" < ts < "08:15:00" or "16:05:00" < ts < "19:30:00"):

        shares = cash / transact_price
        cash = 0
        action = "bto"
        target_price = c_last

    elif shares > 0:

        if h >= target_price :
            action = "stc"
            cash += shares * target_price

        elif transact_price_last > c:
            action = "stc_liquidate_loss"
            cash += shares * c

        else:
            action = "stc_liquidate"
            cash += shares * c

        shares = 0

    v_last = row["v"]
    o_last = row["o"]
    c_last = row["c"]
    h_last = row["h"]
    l_last = row["l"]
    transact_price_last = transact_price

    trade_list += [action]
    cash_list += [cash]
    shares_list += [shares]


df["action"] = trade_list
df["shares"] = shares_list
df["cash"] = cash_list


df.to_csv(TRADE_SIM_FILE, index=False)
print("cash", cash)
print("shares", shares * c)
print()

values, counts = np.unique(trade_list, return_counts=True)
print(values)
print(counts)



