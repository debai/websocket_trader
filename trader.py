import websocket, json, yaml, datetime, os
from ib_insync import *


VARS_FILE = "vars_prod.yaml"
with open(VARS_FILE, "r") as stream:
    vars = yaml.safe_load(stream)

SOCKET = "wss://socket.polygon.io/stocks"
API_KEY = os.environ['polygon_api_key']
ACCOUNT = os.environ['ibkr_account']
cash = vars["cash"]
shares = 0
last_buy_time = None  # Datetime object
last_buy_price = None  # float
last_buy_sym = None  # string, upper case
target_price = None # float
buy_factors = vars["buy_factors"]
ams = vars["ams"] # dict {sym: {am message}}  (AM stands for aggregated minute)
symbols = vars["symbols"]


#################################################
# IBKR API setup
util.startLoop()
ib = IB()
ib.disconnect()
ib.connect('127.0.0.1', 4001, clientId=2)
contracts = {symbol:Stock(symbol, 'SMART', 'USD') for symbol in symbols}
for symbol in contracts:
    ib.qualifyContracts(contracts[symbol])
    ib.reqContractDetails(contracts[symbol])


#################################################
# Trade algo

def buy(am, q):
    global target_price, cash, shares, last_buy_time, last_buy_price, last_buy_sym, buy_factors, contracts

    am_dt = datetime.datetime.fromtimestamp(am["e"]/1000)
    q_dt = datetime.datetime.fromtimestamp(q["t"]/1000)
    qt = q_dt.strftime("%H:%M:%S")

    time_condition = ("04:15:00" < qt < "08:15:00" or "16:05:00" < qt < "19:30:00")
    buy_price = (am["c"] * (1 - buy_factors[am["sym"]]))

    if q_dt.strftime("%M") != am_dt.strftime("%M"):
        return

    # buy condition
    if q["ap"] < buy_price and time_condition and shares == 0:
        action = "bto"

        last_buy_time = q_dt
        shares = int((cash - 10) / buy_price)
        cash = 0
        print(action, shares, int((cash - 10) / buy_price), cash, buy_price)
        order = LimitOrder('BUY', shares, q["ap"], account=ACCOUNT, outsideRth=True)
        ib.placeOrder(contracts[q["sym"]], order)

        last_buy_sym = q["sym"]
        target_price = am["c"]

        last_buy_price = buy_price



def sell(q):
    global target_price, cash, shares, last_buy_time, last_buy_price, last_buy_sym, buy_factor, contracts
    t_m = datetime.datetime.now().strftime("%M")

    #print(t_m,last_buy_time.strftime("%M"), target_price, shares)

    if t_m >= last_buy_time.strftime("%M") and shares > 0:

        if q["bp"] >= target_price:
            ib.reqGlobalCancel()
            action = "stc"
            print(action, shares, )
            shares_copy = int(shares)
            shares = 0
            order = LimitOrder('SELL', shares_copy, q["bp"], account=ACCOUNT,  outsideRth=True)
            ib.placeOrder(contracts[q["sym"]], order)


        # in the next interval or later and needs to liquidate
        elif t_m > (last_buy_time + datetime.timedelta(minutes=1)).strftime("%M") and last_buy_price * (1 - buy_factors[q["sym"]]) < q["bp"]:
            ib.reqGlobalCancel()
            action = "stc_liquidate next minute"
            print(action)
            shares_copy = int(shares)
            shares = 0
            order = LimitOrder('SELL', shares_copy, q["bp"], account=ACCOUNT,  outsideRth=True)
            ib.placeOrder(contracts[q["sym"]], order)



########################################################
# web socket stuff

def on_open(ws):
    auth_data = {"action": "auth", "params": vars["polygon_key"]}
    ws.send(json.dumps(auth_data))
    channel_quotes = ["Q." + symbol.upper() for symbol in symbols]
    channel_ams = ["AM." + symbol.upper() for symbol in symbols]
    subscriptions = {"action": "subscribe","params": ",".join(channel_quotes) + "," + ",".join(channel_ams)}  #",".join(channel_quotes) + "," +
    ws.send(json.dumps(subscriptions))


def on_message(ws, message):
    global ams

    last_msg = json.loads(message)[-1]
    sym = last_msg["sym"]
    ev = last_msg["ev"]

    if ev == "AM":
        ams[sym] = last_msg
        ts = datetime.datetime.fromtimestamp(last_msg["e"]/1000)
        print(ts.strftime("%H:%M:%S"), last_msg)

    elif shares == 0 and ev == "Q":
        #print(last_msg, last_msg)
        buy(ams[sym], last_msg)

    elif sym==last_buy_sym and shares > 0 and ev == "Q" :
        sell(last_msg)


def on_close(ws):
    print("closed connection")


def polygon_ws():
    ws = websocket.WebSocketApp(SOCKET, on_open=on_open, on_message=on_message, on_close=on_close)
    ws.run_forever()


polygon_ws()